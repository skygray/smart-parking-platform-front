import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import store from '../store/index'
import {
  Message
} from 'element-ui';

Vue.use(VueRouter)

const routes = [{
    path: '/',
    redirect: "/login"
  }, {
    path: '/login',
    name: '登录',
    component: Login,
    meta: {
      title: '智慧停车场管理平台'
    },
  }, {
    path: '/index',
    name: '首页',
    component: () => import( /* webpackChunkName: "Index" */ '../views/Index.vue'),
    meta: {
      requiresAuth: true,
      title: '首页'
    },
    children: [{
        path: '/index',
        name: 'Dashbord',
        component: () => import( /* webpackChunkName: "Dashbord" */ '../views/Dashbord.vue'),
        meta: {
          requiresAuth: true,
          title: '首页'
        },
      },
      {
        path: '/charge',
        name: '计费管理',
        component: () => import( /* webpackChunkName: "Charge" */ '../views/Charge.vue'),
        meta: {
          requiresAuth: true,
          title: '计费管理'
        },
        children: [{
            path: '/charge',
            name: '表格',
            component: () => import( /* webpackChunkName: "ChargeTable" */ '../components/ChargeTable.vue')
          },
          {
            path: '/charge/edit/id=:id',
            name: '编辑',
            component: () => import( /* webpackChunkName: "Charge_Edit" */ '../views/Charge_Edit.vue'),
          },
          {
            path: '/charge/add',
            name: '添加',
            component: () => import( /* webpackChunkName: "Charge_Add" */ '../views/Charge_Add.vue'),
          }
        ]
      },
      {
        path: '/distinguish',
        name: '识别记录',
        component: () => import( /* webpackChunkName: "Distinguish" */ '../views/Distinguish.vue'),
        meta: {
          requiresAuth: true,
          title: '识别记录'
        },
        children: [{
          path: '/distinguish',
          name: '表格',
          component: () => import( /* webpackChunkName: "DistinguishTable" */ '../components/DistinguishTable.vue')
        }]
      },
      {
        path: '/record',
        name: '停车记录',
        component: () => import( /* webpackChunkName: "Record" */ '../views/Record.vue'),
        meta: {
          requiresAuth: true,
          title: '停车记录'
        },
        children: [{
          path: '/record',
          name: '表格',
          component: () => import( /* webpackChunkName: "RecordTable" */ '../components/RecordTable.vue')
        }]
      },
      {
        path: '/payment',
        name: '支付记录',
        component: () => import( /* webpackChunkName: "Payment" */ '../views/Payment.vue'),
        meta: {
          requiresAuth: true,
          title: '支付记录'
        },
        children: [{
          path: '/payment',
          name: '表格',
          component: () => import( /* webpackChunkName: "PaymentTable" */ '../components/PaymentTable.vue')
        }]
      },
      {
        path: '/visitor',
        name: '访客记录',
        component: () => import( /* webpackChunkName: "Visitor" */ '../views/Visitor.vue'),
        meta: {
          requiresAuth: true,
          title: '访客记录'
        },
        children: [{
          path: '/visitor',
          name: '表格',
          component: () => import( /* webpackChunkName: "VisitorTable" */ '../components/VisitorTable.vue')
        }]
      },
      {
        path: '/charging',
        name: '充电管理',
        component: () => import( /* webpackChunkName: "Charging" */ '../views/Charging.vue'),
        meta: {
          requiresAuth: true,
          title: '充电管理'
        },
        children: [{
            path: '/charging',
            name: '表格',
            component: () => import( /* webpackChunkName: "ChargingTable" */ '../components/ChargingTable.vue')
          },
          {
            path: '/charging/edit/id=:id',
            name: '编辑',
            component: () => import( /* webpackChunkName: "Charging_Edit" */ '../views/Charging_Edit.vue'),
          },
        ]
      },
      {
        path: '/opinion',
        name: '意见管理',
        component: () => import( /* webpackChunkName: "Opinion" */ '../views/Opinion.vue'),
        meta: {
          requiresAuth: true,
          title: '意见管理'
        },
        children: [{
          path: '/opinion',
          name: '表格',
          component: () => import( /* webpackChunkName: "ChargingTable" */ '../components/OpinionTable.vue')
        }]
      },
      {
        path: '/user',
        name: '用户管理',
        component: () => import( /* webpackChunkName: "User" */ '../views/User.vue'),
        meta: {
          admin: true,
          requiresAuth: true,
          title: '用户管理'
        },
        children: [{
          path: '/user',
          name: '表格',
          component: () => import( /* webpackChunkName: "UserTable" */ '../components/UserTable.vue'),
          children: [{
            path: '/user/add',
            name: '添加用户',
            component: () => import( /* webpackChunkName: "User_Add" */ '../views/User_Add.vue')
          }, {
            path: '/user/edit/id=:id',
            name: '修改用户',
            component: () => import( /* webpackChunkName: "User_Edit" */ '../views/User_Edit.vue')
          }]
        }, ]
      },
      {
        path: '/company',
        name: '合作单位管理',
        component: () => import( /* webpackChunkName: "Company" */ '../views/Company.vue'),
        meta: {
          requiresAuth: true,
          title: '合作单位管理'
        },
        children: [{
          path: '/company',
          name: '表格',
          component: () => import( /* webpackChunkName: "CompanyTable" */ '../components/CompanyTable.vue'),
          children: [{
            path: '/company/add',
            name: '添加合作单位',
            component: () => import( /* webpackChunkName: "Company_Add" */ '../views/Company_Add.vue')
          }, {
            path: '/company/edit/id=:id',
            name: '修改合作单位',
            component: () => import( /* webpackChunkName: "Company_Edit" */ '../views/Company_Edit.vue')
          }]
        }, ]
      },
      {
        path: '/profile',
        name: '个人资料',
        component: () => import( /* webpackChunkName: "Profile" */ '../views/Profile.vue'),
        meta: {
          requiresAuth: true,
          title: '个人资料'
        },
      },
      {
        path: '/profile/edit',
        name: 'Profile_Edit',
        component: () => import( /* webpackChunkName: "Profile_Edit" */ '../views/Profile_Edit.vue'),
        meta: {
          requiresAuth: true,
          title: '个人资料-编辑'
        },
      },
      {
        path: '/private',
        name: '私有停车管理',
        component: () => import( /* webpackChunkName: "Private" */ '../views/Private.vue'),
        meta: {
          requiresAuth: true,
          title: '私有停车管理'
        },
        children: [{
          path: '/private',
          name: '表格',
          component: () => import( /* webpackChunkName: "PrivateTable" */ '../components/PrivateTable.vue'),
          meta: {
            requiresAuth: true,
            title: '私有停车管理-表格'
          },
          children: [{
              path: '/private/edit/id=:id',
              name: '修改私有信息',
              component: () => import( /* webpackChunkName: "Private_Edit" */ '../views/Private_Edit.vue'),
              meta: {
                requiresAuth: true,
                title: '私有停车管理-修改'
              },
            },
            {
              path: '/private/add',
              name: '添加私有信息',
              component: () => import( /* webpackChunkName: "Private_Add" */ '../views/Private_Add.vue'),
              meta: {
                requiresAuth: true,
                title: '私有停车管理-添加'
              },
            }
          ]
        }, ]
      },
      {
        path: '/space',
        name: '停车位管理',
        component: () => import( /* webpackChunkName: "Space" */ '../views/Space.vue'),
        meta: {
          requiresAuth: true,
          title: '停车位管理'
        },
        children: [{
            path: '/space',
            name: '表格',
            component: () => import( /* webpackChunkName: "SpaceTable" */ '../components/SpaceTable.vue'),
            meta: {
              requiresAuth: true,
              title: '停车位管理-表格'
            },
          },
          {
            path: '/space/edit/no=:no',
            name: '修改停车位信息',
            component: () => import( /* webpackChunkName: "Space_Edit" */ '../views/Space_Edit.vue'),
            meta: {
              requiresAuth: true,
              title: '停车位管理-修改'
            },
          },
          {
            path: '/space/add',
            name: '添加停车位',
            component: () => import( /* webpackChunkName: "Space_Add" */ '../views/Space_Add.vue'),
            meta: {
              requiresAuth: true,
              title: '停车位管理-添加'
            },
          },
        ]
      },
      {
        path: '/region',
        name: '区域管理',
        component: () => import( /* webpackChunkName: "Region" */ '../views/Region.vue'),
        meta: {
          requiresAuth: true,
          title: '区域管理'
        },
        children: [{
          path: '/region',
          name: '表格',
          component: () => import( /* webpackChunkName: "RegionTable" */ '../components/RegionTable.vue'),
          meta: {
            requiresAuth: true,
            title: '区域管理-表格'
          },
          children: [{
              path: '/region/edit/id=:id',
              name: '修改停车位信息',
              component: () => import( /* webpackChunkName: "Region_Edit" */ '../views/Region_Edit.vue'),
              meta: {
                requiresAuth: true,
                title: '区域管理-修改'
              },
            },
            {
              path: '/region/add',
              name: '添加区域',
              component: () => import( /* webpackChunkName: "Region_Add" */ '../views/Region_Add.vue'),
              meta: {
                requiresAuth: true,
                title: '区域管理-添加'
              },
            },
          ]
        }, ]
      },
      {
        path: '/bigData_min',
        name: '数据大屏',
        component: () => import( /* webpackChunkName: "BigData_Min" */ '../views/BigData_Min.vue'),
        meta: {
          requiresAuth: true,
          title: '数据大屏'
        },
        children: [{
          path: '/bigData_min',
          name: '准备',
          component: () => import( /* webpackChunkName: "BigDataChart_min" */ '../components/BigDataChart_min.vue'),
          meta: {
            requiresAuth: true,
            title: '数据大屏-准备'
          },
        }]
      }
    ]
  },
  {
    path: '/bigData',
    name: '数据大屏',
    component: () => import( /* webpackChunkName: "BigData" */ '../views/BigData.vue'),
    meta: {
      requiresAuth: true,
      title: '数据大屏'
    },
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  if (to.meta.title) { //判断是否有标题
    document.title = to.meta.title
  }
  // 检测路由配置中是否有requiresAuth这个meta属性
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // 判断是否已登录
    if (store.getters.isLoggedIn) {
      if (to.path == "/user") {
        if (store.getters.isAdmin) {
          next();
          return;
        } else {
          Message.error({
            message: '没有访问权限',
          });
          next('/index');
          return;
        }
      }
      next();
      return;
    }
    // 未登录则跳转到登录界面
    Message({
      message: '请先进行登录',
      type: 'warning'
    });
    next('/');
  } else {
    next()
  }
})

const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

export default router
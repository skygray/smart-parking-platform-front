export function toThousand(str) {
    const num = parseFloat(str.replace(/,/g, ""));
    const toNum = num / 1000;
    return `${toNum}K`;
}
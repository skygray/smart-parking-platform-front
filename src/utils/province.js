export function province(abb) {
    if (abb == "京")
        return "北京";
    else if (abb == "津")
        return "天津";
    else if (abb == "渝")
        return "重庆";
    else if (abb == "沪")
        return "上海";
    else if (abb == "冀")
        return "河北";
    else if (abb == "晋")
        return "山西";
    else if (abb == "辽")
        return "辽宁";
    else if (abb == "吉")
        return "吉林";
    else if (abb == "黑")
        return "黑龙江";
    else if (abb == "苏")
        return "江苏";
    else if (abb == "浙")
        return "浙江";
    else if (abb == "皖")
        return "安徽";
    else if (abb == "闽")
        return "福建";
    else if (abb == "赣")
        return "江西";
    else if (abb == "鲁")
        return "山东";
    else if (abb == "豫")
        return "河南";
    else if (abb == "鄂")
        return "湖北";
    else if (abb == "湘")
        return "湖南";
    else if (abb == "粤")
        return "广东";
    else if (abb == "琼")
        return "海南";
    else if (abb == "川" || abb == '蜀')
        return "四川";
    else if (abb == "黔" || abb == '贵')
        return "贵州";
    else if (abb == "云" || abb == '滇')
        return "云南";
    else if (abb == "陕" || abb == '秦')
        return "陕西";
    else if (abb == "甘" || abb == '陇')
        return "甘肃";
    else if (abb == "青")
        return "青海";
    else if (abb == "台")
        return "台湾";
    else if (abb == "内")
        return "内蒙古";
    else if (abb == "桂")
        return "广西";
    else if (abb == "宁")
        return "宁夏";
    else if (abb == "新")
        return "新疆";
    else if (abb == "藏")
        return "西藏";
    else if (abb == "港")
        return "香港";
    else if (abb == "澳")
        return "澳门";
    return abb;
}
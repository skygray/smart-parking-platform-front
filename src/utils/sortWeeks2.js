export function sortWeeks2(weeksList) {
    const len = weeksList.length;
    for (let i = 0; i < len; i++) {
        if (weeksList[i].weekday === 'Mon') {
            weeksList[i].id = 1;
        }
        if (weeksList[i].weekday === 'Tues') {
            weeksList[i].id = 2;
        }
        if (weeksList[i].weekday === 'Wed') {
            weeksList[i].id = 3;
        }
        if (weeksList[i].weekday === 'Thur') {
            weeksList[i].id = 4;
        }
        if (weeksList[i].weekday === 'Fri') {
            weeksList[i].id = 5;
        }
        if (weeksList[i].weekday === 'Sat') {
            weeksList[i].id = 6;
        }
        if (weeksList[i].weekday === 'Sun') {
            weeksList[i].id = 7;
        }
    }
    return weeksList;
}
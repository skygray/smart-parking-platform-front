export function getNewGoodsList(params) {
    let temp = {};
    for (let i in params) {
        let key = params[i].day; //判断依据
        if (temp[key]) {
            temp[key].day = params[i].day;
            temp[key].money = Number(temp[key].money) + Number(params[i].money); //相加值
        } else {
            temp[key] = {};
            temp[key].day = params[i].day;
            temp[key].money = Number(params[i].money);
        }
    }
    let newArry = [];
    for (let k in temp) {
        newArry.push(temp[k]);
    }
    return newArry;
}
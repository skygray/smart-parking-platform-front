export function pageLoad(url, curpage, lazyPage, count) {
    return new Promise((resolve, reject) => {
        let page = 0;
        curpage < 4 ? page = 1 : page = Math.ceil(curpage / lazyPage)
        this.axios
            .get(url, {
                params: {
                    page: page,
                    count
                }
            })
            .then((res) => resolve(res))
            .catch((err) => {
                reject(err);
            });
    })
}
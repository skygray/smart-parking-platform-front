export function sortWeeks(weeks) {
    let _weeks = [];
    for (let key in weeks) {
        if (key === 'Mon') {
            let _week = {};
            _week["id"] = 1;
            _week["name"] = "Mon";
            _week["value"] = weeks[key];
            _weeks.push(_week);
        }
        if (key === 'Tues') {
            let _week = {};
            _week["id"] = 2;
            _week["name"] = "Tues";
            _week["value"] = weeks[key];
            _weeks.push(_week);
        }
        if (key === 'Wed') {
            let _week = {};
            _week["id"] = 3;
            _week["name"] = "Wed";
            _week["value"] = weeks[key];
            _weeks.push(_week);
        }
        if (key === 'Thur') {
            let _week = {};
            _week["id"] = 4;
            _week["name"] = "Thur";
            _week["value"] = weeks[key];
            _weeks.push(_week);
        }
        if (key === 'Fri') {
            let _week = {};
            _week["id"] = 5;
            _week["name"] = "Fri";
            _week["value"] = weeks[key];
            _weeks.push(_week);
        }
        if (key === 'Sat') {
            let _week = {};
            _week["id"] = 6;
            _week["name"] = "Sat";
            _week["value"] = weeks[key];
            _weeks.push(_week);
        }
        if (key === 'Sun') {
            let _week = {};
            _week["id"] = 7;
            _week["name"] = "Sun";
            _week["value"] = weeks[key];
            _weeks.push(_week);
        }
    }
    _weeks.sort(function (a, b) {
        return a.id - b.id;
    });
    weeks = [];
    for (let i = 0; i < _weeks.length; i++) {
        weeks.push(_weeks[i].value);
    }
    return weeks;
}
//获取浏览器高度
export function PageHeight(height) {
    if (height <= 484) {
        return 3;
    } else if (484 < height && height <= 624) {
        return 5;
    } else if (624 < height && height <= 800) {
        return 7;
    } else if (height >= 1000) {
        return 10;
    }
}
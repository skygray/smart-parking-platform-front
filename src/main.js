import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from './axios/index'

Vue.config.productionTip = false

//按需引入elementui
import 'element-ui/lib/theme-chalk/index.css'
import element from './element/index'
Vue.use(element)

//引入fullpage
import fullpage from "v-fullpage";
Vue.use(fullpage);


//引入animated动画
import animated from 'animate.css'
Vue.use(animated)

//引入DataV
import dataV from '@jiaminghi/data-view'
Vue.use(dataV)

//引入打印模块
import Print from 'vue-print-nb';
Vue.use(Print);

Vue.prototype.axios = axios //挂在axios
Vue.prototype.base_url = 'http://localhost:8080'

//引入懒加载分页
import {
  pageLoad
} from './utils/pageLoad'
Vue.prototype.pageLoad = pageLoad

new Vue({
  router,
  store,
  axios,
  render: h => h(App)
}).$mount('#app')